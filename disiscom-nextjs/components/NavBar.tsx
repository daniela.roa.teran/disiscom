import { FC, useContext, useState } from "react";
import { LayoutContext } from "../context/LayoutTheme";
import Media from "react-media";
import { LanguageContext } from "../context/AppLanguage";
interface Options {
  name: string;
  link: string;
}
interface NavBarProps {
  options: Array<Options>;
}
const NavBar: FC<NavBarProps> = ({ options }) => {
  const { theme, setTheme } = useContext(LayoutContext);
  const { lang, setLang } = useContext(LanguageContext);
  const [menu, setMenu] = useState(false);
  const change_theme = theme.color === "light" ? "DARK" : "LIGHT";
  return (
    <>
      <div className="navigation--container">
        <nav role="visible-menu">
          <button className="menu--button" onClick={() => setMenu(!menu)}>
            <span className="mdi mdi-menu"></span>
          </button>
          <button
            onClick={() => setLang({ type: lang.lang === "ES" ? "EN" : "ES" })}
          >
            Lang: {lang.lang}
          </button>
          <button
            className="theme--toggle"
            onClick={() => {
              setTheme({ type: theme.color === "light" ? "DARK" : "LIGHT" });
            }}
          >
            <span
              className={`mdi ${
                theme.color === "light"
                  ? "mdi-moon-waning-crescent"
                  : "mdi-weather-sunny"
              }`}
            ></span>
            <span>{theme.color === "light" ? "dark" : "light"}</span>
          </button>
          <h1>DISISCOM</h1>
          <Media
            query="(min-width:769px)"
            render={() => (
              <div className="social--container">
                <button>
                  <span className="mdi mdi-facebook"></span>
                </button>
                <button>
                  <span className="mdi mdi-instagram"></span>
                </button>
                <button>
                  <span className="mdi mdi-twitter"></span>
                </button>
                <button>
                  <span className="mdi mdi-linkedin"></span>
                </button>
              </div>
            )}
          />
        </nav>
        <div className={`menu--wrap ${menu ? "opened" : "closed"}`}>
          <ul className="menu--list">
            {options.map((opt, i) => {
              return (
                <li role="nav-option" key={`nav-opt-${i}`}>
                  <a href={opt.link}>{opt.name}</a>
                </li>
              );
            })}
          </ul>
          <Media
            query="(max-width:769px)"
            render={() => (
              <div className="social--container">
                <button>
                  <span className="mdi mdi-facebook"></span>
                </button>
                <button>
                  <span className="mdi mdi-instagram"></span>
                </button>
                <button>
                  <span className="mdi mdi-twitter"></span>
                </button>
                <button>
                  <span className="mdi mdi-linkedin"></span>
                </button>
              </div>
            )}
          />
          <div className="contact--content">
            <div className="contact--item">
              <span className="mdi mdi-email-outline"></span>
              <span className="contact--info">info.disiscom@gmail.com</span>
            </div>
            <div className="contact--item">
              <span className="mdi mdi-phone"></span>
              <span className="contact--info">+34 684 32 57 97</span>
            </div>
          </div>
        </div>
      </div>
      <style jsx>
        {`
          .navigation--container {
            display: flex;
            flex-direction: column;
            position: fixed;
            left: 0;
            top: 0;
            height: 100vh;
            z-index: 9;
          }
          nav {
            display: flex;
            flex-direction: column;
            flex-grow: 1;
            justify-content: space-between;
            width: 80px;
            border-right: 1px solid #3d3e41;
            z-index: 9;
          }
          h1 {
            transform: rotate(-90deg);
          }
          .menu--wrap {
            position: fixed;
            height: 100vh;
            overflow: hidden;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            border-right: 1px solid #3d3e41;
            left: 80px;
          }
          .menu--wrap.opened {
            width: 30vw;
            -webkit-animation: menu-open 0.4s
              cubic-bezier(0.39, 0.575, 0.565, 1) both;
            animation: menu-open 0.4s cubic-bezier(0.39, 0.575, 0.565, 1) both;
          }
          .menu--list {
            list-style: none;
            padding: 0;
          }
          .menu--list li {
            margin: 15px 0;
          }
          .menu--list li a {
            text-transform: uppercase;
            font-size: 20px;
          }
          .menu--wrap.closed {
            width: 0px;
            opacity: 0;
            transition: width 1s ease, opacity 0.5s ease;
          }
          .menu--button {
            border-bottom: 1px solid #3d3e41;
          }
          .menu--button span {
            font-size: 30px;
          }
          .theme--toggle {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          .theme--toggle span:first-child {
            font-size: 25px;
          }
          .social--container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          .social--container button {
            font-size: 25px;
          }
          .contact--content {
            width: 100%;
          }
          .contact--item:first-child {
            border-top: 1px solid #3d3e41;
            border-bottom: 1px solid #3d3e41;
          }
          .contact--item:not(:first-child) {
            border-bottom: 1px solid #3d3e41;
          }
          .contact--item {
            width: 100%;
            justify-content: center;
            padding: 15px;
            display: flex;
            font-size: 14px;
          }
          .contact--info {
            flex-grow: 1;
            margin-left: 20px;
          }
          @keyframes menu-open {
            0% {
              -webkit-transform: scaleX(0.4);
              transform: scaleX(0.4);
              -webkit-transform-origin: 0% 0%;
              transform-origin: 0% 0%;
            }
            100% {
              -webkit-transform: scaleX(1);
              transform: scaleX(1);
              -webkit-transform-origin: 0% 0%;
              transform-origin: 0% 0%;
            }
          }
          @keyframes scale-up-hor-right {
            0% {
              -webkit-transform: scaleX(0.4);
              transform: scaleX(0.4);
              -webkit-transform-origin: 100% 100%;
              transform-origin: 100% 100%;
            }
            100% {
              -webkit-transform: scaleX(1);
              transform: scaleX(1);
              -webkit-transform-origin: 100% 100%;
              transform-origin: 100% 100%;
            }
          }
          @media screen and (max-width: 768px) {
            .navigation--container {
              height: 80px;
              width: 100vw;
              z-index: 9;
            }
            nav {
              flex-direction: row;
              width: 100%;
              border-right: 0;
              padding: 0 15px;
              border-bottom: 1px solid #3d3e41;
              align-items: center;
            }
            .theme--toggle {
              margin-left: 20px;
            }
            h1 {
              transform: rotate(0deg);
              justify-content: center;
              flex-grow: 1;
              text-align: center;
            }
            .menu--button {
              border-bottom: 0;
            }
            .menu--wrap {
              height: calc(100vh - 80px);
              top: 80px;
              left: 0;
            }
            .menu--wrap.opened {
              width: 50vw;
            }
            .contact--content {
              margin-top: 20px;
            }
          }
          @media screen and (max-width: 495px) {
            .menu--wrap.opened {
              width: 100%;
            }
          }
        `}
      </style>
    </>
  );
};
export default NavBar;
