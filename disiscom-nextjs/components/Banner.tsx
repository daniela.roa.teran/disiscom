import { FC } from "react";
import Particles from "react-tsparticles";
const Banner: FC = () => {
  return (
    <>
      <div className="banner--container">
        <Particles
          id="tsparticles"
          options={{
            fpsLimit: 60,
            interactivity: {
              detect_on: "canvas",
              events: {
                onclick: { enable: true, mode: "push" },
                onhover: {
                  enable: true,
                  mode: "repulse",
                  parallax: { enable: false, force: 60, smooth: 10 },
                },
                resize: true,
              },
              modes: {
                bubble: {
                  distance: 400,
                  duration: 2,
                  opacity: 0.1,
                  size: 40,
                  speed: 3,
                },
                grab: { distance: 400, line_linked: { opacity: 1 } },
                push: { particles_nb: 4 },
                remove: { particles_nb: 2 },
                repulse: { distance: 200, duration: 0.4 },
              },
            },
            particles: {
              color: { value: "random" },
              line_linked: {
                color: "random",
                distance: 150,
                enable: true,
                opacity: 0.4,
                width: 1,
              },
              move: {
                attract: { enable: false, rotateX: 600, rotateY: 1200 },
                bounce: false,
                direction: "none",
                enable: true,
                out_mode: "out",
                random: false,
                speed: 3,
                straight: false,
              },
              rotate: {
                animation: {
                  enable: true,
                  speed: 10,
                },
              },
              number: {
                density: { enable: true, value_area: 800 },
                value: 100,
              },
              opacity: {
                anim: { enable: true, opacity_min: 0.5, speed: 1, sync: false },
                random: false,
                value: 1,
              },
              shape: {
                character: [
                  {
                    fill: false,
                    font: "Verdana",
                    style: "",
                    value: "tsParticles".split(""),
                    weight: "400",
                  },
                ],
                image: {
                  height: 100,
                  replace_color: true,
                  src: "/logo_icon.png",
                  width: 100,
                },
                polygon: { nb_sides: 5 },
                stroke: { color: "random", width: 0 },
                type: "image",
              },
              size: {
                anim: { enable: true, size_min: 10, speed: 10, sync: false },
                random: false,
                value: 10,
              },
            },
            polygon: {
              draw: { enable: false, lineColor: "#ffffff", lineWidth: 0.5 },
              move: { radius: 10 },
              scale: 1,
              type: "none",
              url: "",
            },
            retina_detect: true,
          }}
        />
        <h1>We imagine, create and make amazing things real</h1>
      </div>
      <style jsx>{`
        .banner--container {
          height: 100vh;
          width: 100%;
          display: flex;
          flex-direction: column;
          justify-content: center;
          position: relative;
        }
        :global(#tsparticles) {
          height: 100%;
          opacity: 0.3;
        }
        h1 {
          position: absolute;
          text-transform: uppercase;
          font-size: 3em;
          display: flex;
          width: 50vw;
          text-align: center;
          right: 0;
          text-align: end;
          line-height: 65px;
        }
        @media screen and (max-width: 768px) {
          h1 {
            width: 90%;
            font-size: 2.7em;
          }
        }
      `}</style>
    </>
  );
};
export default Banner;
