import { FC, useContext } from "react";
import appTheme from "../styles/appTheme";
import { LayoutContext } from "../context/LayoutTheme";

interface Props {
  children: React.ReactNode;
}

const AppLayout: FC<Props> = ({ children }) => {
  const { theme } = useContext(LayoutContext);
  const style = theme.color === "light" ? appTheme.light : appTheme.dark;
  return (
    <>
      {children}
      <style jsx global>
        {`
          body {
            background-color: ${style.body};
            color: ${style.text};
            font-family: PTMono;
            transition: background-color 1s ease;
          }
          button {
            background: transparent;
            cursor: pointer;
            border: none;
            color: ${style.text};
          }
          .menu--wrap,
          nav {
            background-color: ${style.navigation};
            transition: background-color 1s ease;
          }
          button:focus {
            outline: none;
          }
        `}
      </style>
    </>
  );
};
export default AppLayout;
