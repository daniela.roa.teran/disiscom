import { FC } from "react";

interface Items {
  title: string;
  content: string;
  icon: string;
}
interface BoxProps {
  items: Array<Items>;
}
const Box: FC<BoxProps> = ({ items }) => {
  return (
    <>
      <div className="box--container">
        {items.map((item, i) => {
          return (
            <div className="box">
              <span className={`mdi ${item.icon}`}></span>
              <h3>{item.title}</h3>
              <p>{item.content}</p>
            </div>
          );
        })}
      </div>
      <style jsx>{`
        .box--container {
          display: grid;
          grid-template-columns: repeat(${items.length}, 1fr);
        }
        .box--container .box:not(:first-child) {
          border-top: 1px solid #3d3e41;
          border-right: 1px solid #3d3e41;
          border-bottom: 1px solid #3d3e41;
        }
        .box--container .box:first-child {
          border: 1px solid #3d3e41;
          border-left: 0;
        }
        .box {
          padding: 10px;
          display: flex;
          flex-direction: column;
        }
        .box span {
          font-size: 60px;
          color: #096b78;
          align-self: center;
        }
        @media screen and (max-width: 768px) {
          .box--container {
            grid-template-columns: repeat(${items.length / 2}, 1fr);
          }
        }
        @media screen and (max-width: 425px) {
          .box--container {
            grid-template-columns: 1fr;
          }
        }
      `}</style>
    </>
  );
};
export default Box;
