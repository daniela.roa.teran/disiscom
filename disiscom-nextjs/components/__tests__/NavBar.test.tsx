import { render, cleanup } from "@testing-library/react";
import NavBar from "../NavBar";
import nav_opt from "../../fixtures/navigation_options.json";
afterEach(cleanup);
describe("Navigation menu", () => {
  it("renders the correct list of options", () => {
    const { getAllByRole } = render(<NavBar options={nav_opt} />);
    expect(getAllByRole("nav-option")).toHaveLength(nav_opt.length);
  });
  it("renders each option with the correct text", () => {
    const { getByText } = render(<NavBar options={nav_opt} />);
    expect(getByText("Home")).toBeInTheDocument();
    expect(getByText("Works")).toBeInTheDocument();
    expect(getByText("About")).toBeInTheDocument();
  });
  it("renders fixed menu", () => {
    const { getByRole } = render(<NavBar options={nav_opt} />);
    expect(getByRole("visible-menu")).toBeInTheDocument();
  });
});
