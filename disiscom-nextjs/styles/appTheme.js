const lightTheme = {
  body: "#fafafa",
  text: "#212121",
  navigation: "rgb(250 250 250 / 0.8)",
};

const darkTheme = {
  body: "#212121",
  text: "#fafafa",
  navigation: "rgb(33 33 33 / 0.8)",
};

export default {
  light: lightTheme,
  dark: darkTheme,
};
