import "../styles/globals.css";
import { LayoutTheme } from "../context/LayoutTheme";
import AppLayout from "../components/AppLayout";
import { AppLanguage } from "../context/AppLanguage";
function MyApp({ Component, pageProps }) {
  return (
    <AppLanguage>
      <LayoutTheme>
        <AppLayout>
          <Component {...pageProps} />
        </AppLayout>
      </LayoutTheme>
    </AppLanguage>
  );
}

export default MyApp;
