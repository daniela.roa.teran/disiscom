import Head from "next/head";
import NavBar from "../components/NavBar";
import nav_options from "../fixtures/navigation_options.json";
import Banner from "../components/Banner";
import Box from "../components/Box";
import services_box from "../fixtures/services_box.json";

export default function Home() {
  return (
    <>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="http://cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css"
        />
        <script src="https://cdn.rawgit.com/progers/pathseg/master/pathseg.js"></script>
      </Head>
      <NavBar options={nav_options} />

      <main>
        <Banner />
        <div className="container">
          <Box items={services_box.ES} />
        </div>
      </main>

      <footer></footer>

      <style jsx>{`
        .container {
          margin-left: 80px;
        }
        @media screen and (max-width: 768px) {
          .container {
            margin-left: 0;
          }
        }
      `}</style>
    </>
  );
}
