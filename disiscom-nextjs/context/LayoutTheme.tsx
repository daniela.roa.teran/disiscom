import { createContext, FC, useReducer, Dispatch } from "react";

type Theme = {
  color: string;
};
type Props = {
  children: React.ReactNode;
};
const initialState: Theme = {
  color: "dark",
};

const LayoutContext = createContext<{
  theme: Theme;
  setTheme: Dispatch<any>;
}>({ theme: initialState, setTheme: () => null });

const reducer = (state: Theme, action) => {
  switch (action.type) {
    case "DARK":
      return { ...state, color: "dark" };
    case "LIGHT":
      return { ...state, color: "light" };
    default:
      return state;
  }
};
const LayoutTheme: FC<Props> = ({ children }) => {
  const [theme, setTheme] = useReducer(reducer, initialState);
  return (
    <LayoutContext.Provider value={{ theme, setTheme }}>
      {children}
    </LayoutContext.Provider>
  );
};
export { LayoutTheme, LayoutContext };
