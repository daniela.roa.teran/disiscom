import { createContext, FC, Dispatch, useReducer } from "react";

type Language = {
  lang: string;
};
type Props = {
  children: React.ReactNode;
};
const initialState: Language = {
  lang: "ES",
};
const LanguageContext = createContext<{
  lang: Language;
  setLang: Dispatch<any>;
}>({ lang: initialState, setLang: () => null });

const reducer = (state: Language, action) => {
  switch (action.type) {
    case "EN":
      return { ...state, lang: "EN" };
    case "ES":
      return { ...state, lang: "ES" };
    default:
      return { ...state, lang: initialState.lang };
  }
};

const AppLanguage: FC<Props> = ({ children }) => {
  const [lang, setLang] = useReducer(reducer, initialState);
  return (
    <LanguageContext.Provider value={{ lang, setLang }}>
      {children}
    </LanguageContext.Provider>
  );
};
export { AppLanguage, LanguageContext };
